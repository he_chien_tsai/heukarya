{-# LANGUAGE OverloadedStrings #-}
-- | a List of Dynamics for operating Double value
module AI.Heukarya.Gene.Dynamic.Double where
import AI.Heukarya.Gene.Dynamic
-- | a List of Dynamics for operating Double value
doubleGeneList = [
  toDyn "0" (0.0::Double), toDyn "1" (1.0::Double),
  toDyn "(+)" ((+)::Double->Double->Double),
  toDyn "(-)" ((-)::Double->Double->Double),
  toDyn "(*)" ((*)::Double->Double->Double),
  toDyn "(/)" ((/)::Double->Double->Double),
  toDyn "(**)" ((**)::Double->Double->Double),
  toDyn "flip" (flip::(Double->Double->Double)->Double->Double->Double),

  toDyn "exp" (exp::Double->Double),
  toDyn "log" (log::Double->Double),
  toDyn "sin" (sin::Double->Double),
  toDyn "cos" (cos::Double->Double),
  toDyn "asin" (asin::Double->Double),
  toDyn "acos" (acos::Double->Double),

  toDyn "sinh" (sinh::Double->Double),
  toDyn "cosh" (cosh::Double->Double),
  toDyn "asinh" (asinh::Double->Double),
  toDyn "acosh" (acosh::Double->Double)
  ]
