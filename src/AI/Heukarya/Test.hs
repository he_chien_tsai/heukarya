{-# LANGUAGE OverloadedStrings#-}
module AI.Heukarya.Test where
import AI.Heukarya.Gene.Dynamic
import AI.Heukarya.Gene.Dynamic.Double
import AI.Heukarya.Jungle

import System.Random
import Data.Sequence as S
import Data.List as L

testGene = [toDyn "flip" (flip::(Int->Int->Int)->Int->Int->Int),toDyn "negate" (negate::Int->Int),toDyn "(+)" ((+)::Int->Int->Int),toDyn "10" (10::Int),toDyn "5" (5::Int), toDyn "(*)" ((*)::Int->Int->Int)]
evalSort = \x-> S.sort $  evalJungle x

testJungle1 =
  genJungle (mkStdGen 10) 5 testGene "Int" 100
testCrossed11 =
  crossJungle (mkStdGen 149562) 5 testJungle1 0
testCrossed12 =
  crossJungle (mkStdGen 12) 5 testJungle1 1
testCrossed13 =
  crossJungle (mkStdGen 12) 5 testJungle1 0.5
testMutated11 =
  mutateJungle  (mkStdGen 575783) 5 testGene testJungle1 0
testMutated12 =
  mutateJungle  (mkStdGen 13) 5 testGene testJungle1 1
testMutated13 =
  mutateJungle  (mkStdGen 13) 5 testGene testJungle1 0.5
test1 = evalSort testJungle1 == evalSort testCrossed11 && evalSort testJungle1 == evalSort testMutated11

testJungle2 =
  genJungle (mkStdGen 10) 5 testGene "Int -> Int" 100
testCrossed21 =
  crossJungle (mkStdGen 149562) 5 testJungle2 0
testCrossed22 =
  crossJungle (mkStdGen 358112) 5 testJungle2 1
testCrossed23 =
  crossJungle (mkStdGen 2154362) 5 testJungle2 0.5
testMutated21 =
  mutateJungle  (mkStdGen 575783) 5 testGene testJungle2 0
testMutated22 =
  mutateJungle  (mkStdGen 13) 5 testGene testJungle2 1
testMutated23 =
  mutateJungle  (mkStdGen 13) 5 testGene testJungle2 0.5
test2 = evalSort testJungle2 == evalSort testCrossed21 && evalSort testJungle2 == evalSort testMutated21
