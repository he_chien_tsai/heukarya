# HEuyarka 
## A Strong Typed Genetic Programming Library
  It based on Data.Dynamic and tree container, so that Gene can represent both haskell functions and syntax trees by type connstructors. since it also supports higher order functions by multiple way type parsing, there's no need to have any variable in Gene's Tree structure.


* AI.Heukarya.Gene : Abstract Gene manipulation and Typeclass
* AI.Heukarya.Jungle : Operating Collections of Gene
* AI.Heukarya.Center : Module which wraps main functionality for library using

* AI.Heukarya.Gene.Dynamic : one of Gene's implementation. 
  since it's impossible to save checkpoints if using Data.Dynamic directly,
  a symbolic wrapper around dynamic would be helpful
* AI.Heukarya.Gene.Dynamic.Double : a sample geneList of operators of Double precision numbers

TODO :

* AI.Heukarya.Island : let creatures can across the sea between islands. flood fill points' ip address through points, distributed computing.

* maybe use 'plugins' and 'haskell-src' to evolve haskell codes

<a href="https://flattr.com/submit/auto?user_id=hashcat&url=https://github.com/t3476/heukarya/&title=Genetic%20programming%20in%20Haskell&hidden=1&category=software"><img src="https://api.flattr.com/button/flattr-badge-large.png" alt="Flattr Heukarya" /></a>
