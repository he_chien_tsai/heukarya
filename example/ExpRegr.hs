{-# LANGUAGE OverloadedStrings #-}
module Main where
import AI.Heukarya.Center
import AI.Heukarya.Gene.Dynamic
import AI.Heukarya.Gene.Dynamic.Double
import System.Random
import Data.Sequence as S
import qualified Data.Foldable as F

main = finalEcoSystem >>= putStrLn . show . ecoJungle 

ecoSystem = 
  initGeneration 
    EcoUnWritable{
      rndGen = (mkStdGen 12345),
      genes  = doubleGeneList
    }
    defaultEcoConfig{
      outType = "Double -> Double"
    }

sampleTable = [(x,exp (3*x)) | x <- [0,0.1..10]]
fitnessFunc = \tree ->
  let func = fromDyn (evalTreeGene tree) (\_->1/0)::Double -> Double 
  in (sum $ map ( \(x,y) -> 1/(1 + abs (func x - y)) ) sampleTable)
     / (fromIntegral .Prelude.length) sampleTable
finalEcoSystem = 
  evolveCheckGeneration 100000 "ExpReg.EcoSystem" 100
    ecoSystem (\jungle->or $ F.toList $ S.mapWithIndex (\_ x->fitnessFunc x <= 0) jungle) fitnessFunc 0
